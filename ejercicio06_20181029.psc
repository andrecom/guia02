Funcion tpg <- telapulgadas (telametros)
	pg = (1/0.0254)		// pulgada=0,0254 metros
	tpg=telametros*pg
	//	o también 
	//	tpg=telametros*(pg/1)
	
Fin Funcion

Algoritmo Ejercicio06	
	//	6.-  Una modista, para realizar sus prendas de vestir, encarga las telas
	//	al extranjero. Para cada pedido, tiene que proporcionar las medidas de
	//	la tela en pulgadas, pero ella generalmente las tiene en metros.
	//	Realice un algoritmo para ayudar a resolver este problema, determinando
	//	cuantas pulgadas debe pedir con base en los metros que requiere.
	//	Represéntatelo mediante el diagrama de flujo y el pseudocódigo (1 pulgada = 0,0254 m).
	
	Escribir "cuántos metros de tela tienes? Indique en metros"
	Leer telametros
	
	Escribir "La cantidad de tela que tienes en pulgadas son " telapulgadas(telametros) " pulgadas."
	
FinAlgoritmo
