Funcion dinero_x_galon = dinero_total(pesos_x_galon,litros_entregados)
	galon=3.785									// 3.785 litros
//	litro=1/galon								// 0,2642007926023778 galon
	galon_entregado = litros_entregados/galon		// valor de equivalencia de litros a galones
	dinero_x_galon = galon_entregado*pesos_x_galon	// NO OLVIDAR INCLUIR EL NOMBRE DE ESTA VARIABLE COMO VARIABLE A RETORNAR!!!
													// En este caso, "dinero_total".
FinFuncion

Algoritmo Ejercicio04
	//	Un productor de leche lleva el registro de lo que produce en litros
	//	pero cuando entrega le pagan en galones. Realice un algoritmo, y repres�ntelo
	//	mediante un diagrama de flujo y el pseudoc�digo, que ayude al productor a saber cuanto
	//	recibir� por la entrega de su producci�n de un d�a (1 galon = 3,785 litros)
	//	Utilice funci�n "Funcion variable_de_retorno <- Nombre ( Argumentos )"
	
//	Definir pesos_x_galon como entero
	//	Definir litros_entregados como numero

	
	Escribir "�Cu�nto litros entregas al d�a?"
	Leer litros_entregados
	
	Escribir " �A cuanto te pagan el gal�n de leche?"
	Leer pesos_x_galon	
	
	Escribir "El valor del gal�n es de 1000 pesos"
	Escribir "Por lo tanto, recibir�s " dinero_total(pesos_x_galon,litros_entregados) "."
	
	
FinAlgoritmo
