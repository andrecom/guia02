Funcion area_rectangulo = rectangulo (a,b)
	area_rectangulo=(a*b)
Fin Funcion

Algoritmo Ejercicio02
	//	Se requiere conocer el �rea de un rect�ngulo. Realice un algoritmo para tal n y
	//	repres�ntelo mediante un diagrama de flujo, y el pseudoc�digo para realizar este proceso
	//	Como se sabe, para poder obtener el �rea del rect�ngulo, primeramente
	//	se tiene que conocer la base y la altura y una vez obtenidas se presenta el resultado.
	//	Utilice funci�n "Funcion variable_de_retorno <- Nombre ( Argumentos )"
	
	Escribir "Ingrese el valor de la base"
	Leer valorbase
	
	Escribir "Ingrese el valor de la altura"
	Leer valoralt
	
	Escribir "El �rea de tu rect�ngulo es " rectangulo(valorbase,valoralt) "." // no olvidar incluir los argumentos (variables)
																				//junto al nombre de la funci�n.
	
FinAlgoritmo
