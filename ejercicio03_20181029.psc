Funcion area_circulo = Area(radio)
	radio_cuadrado = radio * radio //variable radio al cuadrado
	area_circulo = PI*radio_cuadrado
	
FinFuncion

Algoritmo Ejercicio03
	//	Se requiere obtener el �rea de una circunferencia. Realizar el algoritmo correspondiente y representarlo
	//	mediante un diagrama de flujo y el pseudocodigo correspondiente.
	//	(A = p r�)
	//	Utilice funci�n "Funcion variable_de_retorno <- Nombre ( Argumentos )"
	
	Escribir " Ingrese el valor del radio del c�rculo"
	Leer radio
		Escribir " El �rea del c�rculo es  " Area(radio) "."
	
FinAlgoritmo
